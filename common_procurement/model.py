"""Model for common parameters
"""
from dataclasses import dataclass
from enum import Enum



class Status(Enum):
    """_class for assigning status using Enum_

    Args:
        Enum (_type_): _description_
    """
    NOT_PROCESSED = 0
    APPROVED = 1
    DECLINED = 2
    PROCESSING = 3
    PROCESSED = 4


@dataclass
class CommonRequest:
    """_data class for requisitions_
    """
    item_name: str
    description : str
    request_type: str
    quantity : int
    item_code: int
    user_dept: str
    status: Status = 'NOT_PROCESSED'