import requests

class ProcurementAPIs:

 
    url = 'http://127.0.0.1:5000'

    def __init__(self):
        pass
 
    def get_all_resources(self):
        try:
            response = requests.get(self.url+'/requisitions')
            if response.ok:
                return response.text
            raise Exception
        except Exception as exc:
            print(exc)


    def get_resource_id(self,id):
        '''method to retrieve information if client request for 
        resource using id
        '''
        try:
            response = requests.get(self.url+'/requisitions/'+str(id))
            if response.ok:
                return response.text
            raise Exception
        except Exception as exc:
            print(exc)
    
    def get_all_items_dept(self,user_dept):
        '''method to retrieve all items in a user_dept(engineering,operations)
        '''
        try:
            response = requests.get(self.url+'/requisitions/'+ user_dept)
            if response.ok:
                return response.text
            raise Exception
        except Exception as exc:
            print(exc)

    def get_item_dept(self,item_name,user_dept):
        '''method to retrieve an item(eg.computer)in a user_dept
        '''
        try:
            response = requests.get(self.url +'/requisitions/'+ item_name +'/'+ user_dept)
            if response.ok:
                return response.text
            raise Exception
        except Exception as exc:
            print(exc)

    def get_request_type_dept(self,request_type,user_dept):
        '''method to retrieve type of request(goods/service)from a dept
        '''
        try:
            response = requests.get(self.url+'/requisitions/'+ request_type +'/'+ user_dept)
            if response.ok:
                return response.text
            raise Exception
        except Exception as exc:
            print(exc)

    def post_payload(self,payload):
        '''method to post payload to DB
        '''
        try:
            response = requests.post(self.url+'/requisitions', json=payload)
            if response.ok:
                return f'Insert was successful'
            raise Exception
        except Exception as exc:
            print(exc)
    
    def delete_resource(self,id):
        '''method to delete payload using record no(id)
        '''
        try:
            response = requests.delete(self.url+'/requisitions/'+str(id))
            if response.ok:
                return f'delete was successful at row {id}'
            raise Exception
        except Exception as exc:
            print(exc)

    def update_status_resource(self,payload,id):
        '''method to update entry status using record no(id)
        '''
        try:
            response = requests.put(self.url+'/requisitions/status/'+str(id), json=payload)
            if response.ok:
                return f'update was successful'
            raise Exception
        except Exception as exc:
            print(exc)
    

request_object = ProcurementAPIs()
print(request_object.get_all_resources())
print(request_object.get_resource_id(1))
#print(request_object.delete_resource(6))
payload = {
    "description": "cables",
    "item_code": "CBL 800",
    "item_name": "Cable",
    "quantity": 20,
    "request_type": "goods",
    "status": "PROCESSED",
    "user_dept": "O&M"
  }

print(request_object.update_status_resource(payload,7))
print(request_object.post_payload(payload))


