from dataclasses import dataclass
from enum import Enum

class Status(str,Enum):
   ERROR = 1
   SUCCESS = 2
   DEFECTIVE = 3
   GOOD = 4
   
@dataclass
class ReqMessage:
    #productId: int
    productType: str
    departmentName: str
    requestedBy: str
    description: str
    status: Status
